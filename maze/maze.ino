#include <Arduino.h>
#include <QTRSensors.h>
#include "defines.h"
#include <EEPROM.h>

#define NUM_SENSORS 8


QTRSensorsRC qtrrc((unsigned char[]) {S1, S2, S3, S4, S5, S6, S7, S8}, NUM_SENSORS, TIMEOUT, EMITTER_PIN);
QTRSensorsRC left_right((unsigned char[]) {LEFT_SEN, RIGHT_SEN}, 2, 2500);
unsigned int sensorValues[NUM_SENSORS];
float I=0;
int past_error = 0;
short int turns[200];
int iter=0;
int n = 0;
bool opt=false;
bool left=false, right=false;
unsigned int left_right_values[2];

void setup() {
	Serial.begin(9600);
	pinMode(2, OUTPUT);
	digitalWrite(2, LOW);
	pinMode(S1, INPUT);
	pinMode(S2, INPUT);
	pinMode(S3, INPUT);
	pinMode(S4, INPUT);
	pinMode(S5, INPUT);
	pinMode(S6, INPUT);
	pinMode(S7, INPUT);
	pinMode(S8, INPUT);
	pinMode(LEFT_SEN, INPUT);
	pinMode(RIGHT_SEN, INPUT);

	pinMode(MOT1_VIN, OUTPUT);
	pinMode(MOT1_GND, OUTPUT);
	pinMode(MOT2_VIN, OUTPUT);
	pinMode(MOT2_GND, OUTPUT);

	if(opt){
		unsigned short int k = 0;
		while(turns[k-1]!=5){
			turns[k] = EEPROM.read(k);
			k++;
		}
		n = k-1;
		optimise();
	}
	for(int i=0; i<200; i++){
			qtrrc.calibrate();
			left_right.calibrate();
	}
	qtrrc.read(sensorValues);
	Serial.println("Before wait");
	while(true){
		// qtrrc.read(sensorValues);
		left_right.readLine(left_right_values);
		// int position = qtrrc.readLine(sensorValues);
		for(int i=0; i<2; i++){
			// Serial.println(position);
			Serial.print(left_right_values[i]); Serial.print(' ');
		}
		Serial.println();
	}
	do{
		qtrrc.read(sensorValues);
		Serial.print(sensorValues[0]); Serial.print(" "); Serial.println(sensorValues[5]);
	}while(sensorValues[0]!=2500 && sensorValues[4]!=2500 && sensorValues[7]!=2500);
	do{
		qtrrc.read(sensorValues);
		Serial.print(sensorValues[0]); Serial.print(" "); Serial.println(sensorValues[5]);
	}while(sensorValues[0]==2500 && sensorValues[5]==2500);
	Serial.println("After wait");
	// delay(200);
	// center();
	// go(0, 0);
	// delay(1000);
	while(1);
	// while(1){
	// 	int position = qtrrc.readLine(sensorValues);
	// 	PID(position);
	// }
}

void loop() {
	// int position = qtrrc.readLine(sensorValues);
	// static short int intersection = 0;
	// static int itter = 0;
	// back_sensors();
	// if (!opt){
	// 	if(left || right){
	// 		go(BASE_SPEED, BASE_SPEED);
	// 		while(left || right){
	// 			back_sensors();
	// 			if(left){
	// 				intersection = LEFT;
	// 				break;
	// 			}
	// 			else if(right){
	// 				intersection = FORW;
	// 			}
	// 		}
	// 		unsigned long int last_time = millis();
	// 		short int delta = 0;
	// 		while(delta < 250){
	// 			delta = millis()-last_time;
	// 			if(delta > 100 && delta < 150){
	// 				qtra.read(turn_sensors);
	// 				if(turn_sensors[0] > TRESHOLD_VALUE)
	// 					turn(STOP);
	// 			}
	// 			if(position < 2500 || position > 5000){
	// 				go(BASE_SPEED, BASE_SPEED);
	// 			}
	// 			else{
	// 				position = qtrrc.readLine(sensorValues);
	// 				PID(position);
	// 			}
	// 		}
	// 		position = qtrrc.readLine(sensorValues);
	// 		back_sensors();
	// 		if((position == 0 || position == 7000) && intersection == FORW)
	// 			intersection = RIGHT;
	// 		turn(intersection);
	// 	}
	// 	else{
	// 		if((position == 0 || position == 7000)){
	// 			intersection = BACK;
	// 			turn(intersection);
	// 			center();
	// 		}
	// 		else PID(position);
	// 	}
	// }


	// else{

	// 	if(left || right ){
	// 		go(BASE_SPEED, BASE_SPEED);
	// 		if(turns[iter]!=STOP)
	// 			while(left || right){
	// 				back_sensors();
	// 			}
	// 		unsigned long int last_time = millis();
	// 		short int delta = 0;
	// 		while(delta < 150){
	// 			delta = millis()-last_time;
	// 			if(delta > 50 && delta < 80){
	// 				back_sensors();
	// 				if(left || right)
	// 					turn(STOP);
	// 			}
	// 			if(position < 2500 || position > 5000){
	// 				go(BASE_SPEED, BASE_SPEED);
	// 			}
	// 			else{
	// 				position = qtrrc.readLine(sensorValues);
	// 				PID(position);
	// 			}
	// 		}
	// 		position = qtrrc.readLine(sensorValues);
	// 		turn(turns[iter]);
	// 		iter++;
	// 	}
	// 	else{
	// 		PID(position);
	// 	}
	// }
}

void back_sensors(){
	unsigned int follow_sensors[8];
	
	qtrrc.read(follow_sensors);
	if(follow_sensors[0] < 1000) right = false;
	else right = true;
	if(follow_sensors[7] < 1000) left = false;
	else left = true;
}

void turn(short int intersection){
	static short int k=0;
	if(!opt)
		turns[k++] = intersection;

	switch(intersection){
		case LEFT:
			go(-TURN_SPEED, TURN_SPEED);
			do{
				back_sensors();
			}while(!left);
			delay(10);
			do{
				back_sensors();
			}while(left);
			delay(10);
			center();
			break;
		case RIGHT:
			go(TURN_SPEED, -TURN_SPEED);
			do{
				back_sensors();
			}while(!right);
			delay(10);
			do{
				back_sensors();
			}while(right);
			delay(10);
			center();
			break;
		case BACK:
			go(BASE_SPEED, BASE_SPEED);
			delay(200);
			break;
		case FORW:
		
			break;
		case STOP:
			go(0,0);
			if(!opt){
				n = k;
				k=0;
				do{
					EEPROM.update(k, turns[k]);
					k = k + 1;
				}while (turns[k-1]!=STOP);
			}
			while(1);
			break;
	}
}

void swap(short int &x, short int &y)
{
	int aux = x;
	x=y;
	y=aux;
}

void optimise()
{
    for (int i=0; i<n; i++)
    {
        if (turns[i]==0)
        {
            while(turns[i]==0)
            {
                turns[i]=(turns[i+1]+turns[i-1])%4;
                for (int k=i-1; k<n; k++)
                {
                    swap(turns[k],turns[k+1]);
                }
                n--;
                i--;
                for (int k=i+1; k<n; k++)
                {
                    swap(turns[k],turns[k+1]);
                }
                n--;
            }
        }
    }
}

void PID(int position){
	int error = SET_POINT - position;
	int correction=KP*error+KI*I+KD*(error-past_error);
	go(BASE_SPEED+correction, BASE_SPEED-correction);
	past_error=error;
	I=I*0.7+error;
}

void center(){
	int error, correction, last_error=0;
	float integral=0;
	do{
		Serial.print("error\t"); Serial.print(error);

		error = SET_POINT - qtrrc.readLine(sensorValues);
		correction = error*KP+integral*KI+(error-last_error)*KD;
		if (correction > -40 && correction < 40)
			correction = correction * 2;
		if(correction > BASE_SPEED) correction = BASE_SPEED;
		if(correction < -BASE_SPEED) correction = -BASE_SPEED;
		go(correction, -correction);
		integral = integral*0.7+error;
		last_error = error;
	}while(integral > 167 || integral < -167);
}

void go(int speedLeft, int speedRight) {
	speedLeft = speedLeft*0.95;
	if(speedLeft  >  MAX_SPEED)	speedLeft	=  MAX_SPEED;
	if(speedLeft  < -MAX_SPEED)	speedLeft	= -MAX_SPEED;
	if(speedRight >  MAX_SPEED)	speedRight	=  MAX_SPEED;
	if(speedRight < -MAX_SPEED)	speedRight	= -MAX_SPEED;

	if (speedLeft > 0){
		analogWrite(MOT1_VIN, speedLeft);
		digitalWrite(MOT1_GND, LOW);
	} 
	else {
		analogWrite(MOT1_VIN, 255+speedLeft);
		digitalWrite(MOT1_GND, HIGH);
	}
 
	if (speedRight > 0){
		analogWrite(MOT2_VIN, speedRight);
		digitalWrite(MOT2_GND, LOW);
	}
	else{
	    analogWrite(MOT2_VIN, 255+speedRight);
	    digitalWrite(MOT2_GND, HIGH);
    }
}
