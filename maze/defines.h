#define KP 0.02
#define KD 0.15
#define KI 0.005
#define BASE_SPEED 70
#define	MIN_SPEED 40
#define MAX_SPEED 155
#define	TURN_SPEED	50
#define TRESHOLD_VALUE	700
#define	LEFT 	1
#define RIGHT 	3
#define FORW	2
#define BACK	0
#define STOP 	5
#define TIMER	100

#define MOT2_VIN  3
#define MOT2_GND  4
#define MOT1_VIN  6
#define MOT1_GND  9
#define LEFT_SEN  5
#define RIGHT_SEN 8

#define SET_POINT 3500

#define NUM_SENSORS   8
#define TIMEOUT       2500
#define EMITTER_PIN   7 
#define S1 10
#define S2 11
#define S3 12
#define S4 13
#define S5 A0
#define S6 A1
#define S7 A2
#define S8 A3
// #define VIN 4
// #define LEFT_SEN A6
// #define RIGHT_SEN A7
#define LED	13

// #define FIRST_DELAY