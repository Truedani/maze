#include <Arduino.h>
#include <QTRSensors.h>
#include "defines.h"
#include <EEPROM.h>

#define NUM_SENSORS 8


QTRSensorsRC qtrrc((unsigned char[]) {S1, S2, S3, S4, S5, S6, S7, S8}, NUM_SENSORS, TIMEOUT, EMITTER_PIN);
QTRSensorsAnalog qtra((unsigned char[]) {6, 7},
  2, 1);
unsigned int sensorValues[NUM_SENSORS];
unsigned int turn_sensors[2];

void setup() {
	Serial.begin(9600);
	pinMode(LED, OUTPUT);
	pinMode(MOT1_VIN, OUTPUT);
	pinMode(MOT1_GND, OUTPUT);
	pinMode(MOT2_VIN, OUTPUT);
	pinMode(MOT2_GND, OUTPUT);
    sides();
	// qtrrc.read(sensorValues);
	// while(sensorValues[0]!=2500 && sensorValues[4]!=2400 && sensorValues[7]!=2500){
	// 	qtrrc.read(sensorValues);
	// }

}

void loop(){
    // sides();
    // Serial.print(turn_sensors[0]); Serial.print(" "); Serial.print(turn_sensors[1]);
    qtra.read(turn_sensors);
    // Serial.print(" "); Serial.print(turn_sensors[0]); 
    Serial.print(" "); Serial.println(turn_sensors[1]);
    // delay(500);
}

void sides(){
    static int min_left=700, max_left=1000;
    static int min_right=700, max_right=1000;
    static int avg_left = (min_left + max_left)/3;
    static int avg_right = (min_right + max_right)/3;
    static bool first = false;
    static int left=0, right=0;
    // static int I_L = 0, I_R = 0;
    if(first){
        digitalWrite(LED, HIGH);
        for(int i=0; i<200; i++){
                qtrrc.calibrate();
                qtra.read(turn_sensors);
                if(min_left > turn_sensors[0]) min_left = turn_sensors[0];
                if(max_left < turn_sensors[0]) max_left = turn_sensors[0];
                if(min_right > turn_sensors[1]) min_right = turn_sensors[1];
                if(max_right < turn_sensors[1]) max_right = turn_sensors[1];
            }
        avg_left = (min_left+max_left)/4;
        avg_right = (min_right+max_right)/4;
        digitalWrite(LED, LOW);
        first = false;
    //     Serial.print("min_left "); Serial.print(min_left);
    //     Serial.print("avg_left "); Serial.print(avg_left);
    //     Serial.print("max_left "); Serial.println(max_left);
    //     Serial.print("min_right "); Serial.print(min_right);
    //     Serial.print("avg_right "); Serial.print(avg_left);
    //     Serial.print("max_right "); Serial.println(max_left);
    }
    else{
        qtra.read(turn_sensors);
        if(turn_sensors[0] < 850){
            left = 0;
        }
        if(turn_sensors[0] > 900){
            left = 1;
        }
        if(turn_sensors[1] < 850){
            right = 0;
        }
        if(turn_sensors[1] > 900){
            right = 1;
        }
        turn_sensors[0] = left;
        turn_sensors[1] = right;
    }
}